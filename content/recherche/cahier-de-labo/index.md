---
title: Cahier de labo
subtitle: Là où on scribe tout
date: 2015-02-20
tags: ["example", "markdown"]
---

You can write regular [markdown](http://markdowntutorial.com/) here and Jekyll will automatically convert it to a nice webpage.  I strongly encourage you to [take 5 minutes to learn how to write in markdown](http://markdowntutorial.com/) - it'll teach you how to transform regular text into bold/italics/headings/tables/etc.

**Here is some bold text**

## Here is a secondary heading

Here's a useless table:

| **BAssin 1** | Bassin 2 | Bassin 3 | Previous number |
| :------ |:--- | :--- | :--- |
| Five | Six | Four | Four |
| Ten | Eleven | Nine | Four |
| Seven | Eight | Six | Four |
| Two | Three | One | Four |

and another one from a csv file in data :

{{< get-csv sep="," url="static/tables/test.csv" >}}  



|FIELD1        |Date de la mise en culture|Date de la prise de mesure|Heure de la prise de mesure|personne qui a pris les mesures|personne qui a documenté|ph |%brix|°C liquide|°C air|Observation|Action|
|--------------|--------------------------|--------------------------|---------------------------|-------------------------------|------------------------|---|-----|----------|------|-----------|------|
|cornifletteone|2/9/22                    |2/9/22                    |11:44                      |Calam                          |Calam                   |7  |20   |24        |24    |           |      |
|jessica4ever  |04/03/2022                |                          |                           |                               |                        |   |     |          |      |           |      |


numéro de culture,1,1,1,1,1,1,1,1,1
numéro d’échantillon,1,2,3,4 (Troué 1),5 (Troué 2),6,7,8,9
date de mise en pousse,,,,,,,,,
infusion,,,,,,,,,
sucre,,,,,,,,,
cellulose,,,,,,,,,
pourcentage de starter,,,,,,,,,
date de fin de pousse,,,,,,,,,
régularité de l’épaisseur en sortie de bassin,,,,,,,granuleux,,
quantification de l’épaisseur en sortie de bassin,,,,,,épais,fin,,
division en plusieurs couches en sortie de bassin,,,,,,,,,
contamination pendant la pousse,,,,,,,,,"contaminé, tâches noires incrustées"
lavage,Soude 5 min,Soude 1h,,à l’eau,à l’eau,à l’eau,à l’eau,à l’eau,à l’eau
date de trempage,,,,,,,,,
trempage,eau,eau,eau,Colorant alimentaire rouge (pot entier) ,bleu foncé (pot entier) dans 5L d’eau,"bleu clair (pot entier) dans 5L d’eau, trempage avec le 7",Colorant alimentaire bleu clair  (pot enrier dans 5L d’eau) trempage avec le 6,eau,eau
temps de trempage,1h,1h,2 jours,1 nuit  ,1 nuit,1 nuit,1 nuit,1 nuit,1 nuit
Temps d’égoutage,,Essorage 1 nuit,Essorage 5 h,,,,,,
date de mise en séchage,Le 28/11 ,Le 8 /12,Le 13/12 acab,,,,,,
température moyenne de l’air en séchage,appart froid + terrasse + 5 % + appart marie chauffé,,,,,,,,
température maximale de l’air en séchage,,,,,,,,,
température minimale de l’air en séchage,,,,,,,,,
mode de séchage,plastique alimentaire (couvercle de bassin),plastique alimentaire (couvercle de bassin),plastique alimentaire (couvercle de bassin),plastique alimentaire (couvercle de bassin),plastique alimentaire (couvercle de bassin),plastique alimentaire (couvercle de bassin),plastique alimentaire (couvercle de bassin),tapis chauffant sous plastique alimentaire (couvercle de bassin),plastique alimentaire (couvercle de bassin)
Aspect général avant séchage,,Cristaux de bruliure blancs après la soude,,,,,,,
,,bulles après essorage,,,,,,,
,,cristaux blancs disparus après essorga ,,,,,,,
Aspect général pendant séchage,,bulles disparaissent pendant le séchage ,,,,,,,
décollage ou retournement pendant séchage,,,,,,,,,
collant après séchage,oui,,,,,,,,
opacité après séchage,"plus foncé, orangé au centre",,,,,,,,
tâches après séchage,,,,,,,,,
effet cartoné après séchage,,,,,,,,,
retractation ou lissage après séchage,,,,,,,,,
piquetage après séchage,,,,,,,,,
insectes après séchage,,,,,"insecte blanc, rond, se déplaçant (photo : lien)",,,,
odeur après séchage,"odeur de vinaigre très faible, légère odeur de caramel",,,,,,,,
manufacture,,,gravure,,,,,,
traitement,,,,,,,,,


How about a yummy crepe?

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

Here's a code chunk with syntax highlighting:

```javascript
var foo = function(x) {
  return(x + 5);
}
foo(3)
```
