![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

# Créer une nouvelle page
Aller dans le dossier content/, puis dans le sous-dossier de votre choix (c-ki pour votre présentation). Créer un nouveau dossier dans ce sous-dossier. Le nom que vous donnez au nouveau dossier définit l'url qui sera attribuée à votre page, le nom ne doit donc pas contenir d'espace, de dièse, de slash, bref d'une manière générale aucun caractère spécial. Dans ce nouveau dossier, créer un fichier nommé index.md. Ce fichier contient tout ce qui sera affiché sur votre page. Il est au format [markdown](https://docs.framasoft.org/fr/grav/markdown.html).  

Vous pouvez ajouter une image en la plaçant dans votre dossier et en l'appelant dans votre fichier index.md avec la syntaxe suivante : `![texte de description](nom_de_limage.extension)`.
Par exemple pour une image nommée selfie.jpg, ça donnerait `![Mon super selfie](selfie.jpg)`.
Vous pouvez aussi reprendre l'exemple de [cette page](https://gitlab.com/myxonautes/komb/-/tree/master/content/c-ki/drloiseau/index.md) pour inclure une image encadrée par du texte.  

# Afficher un tableau enregistré au format csv
Un exemple de rendu est visible [ici](komb/cahier-de-labo/manufacture/), et le fichier source [ici](https://gitlab.com/myxonautes/komb/-/tree/master/content/cahier-de-labo/manufacture/index.md).
Un tableau csv (comma separated values) est un simple fichier texte, structuré par des virgules. Chaque virgule définit une nouvelle colonne, et chaque retour à la ligne une nouvelle ligne du tableau.
Pour afficher un tel tableau sur le site, on appelle un "shortcode" avec la syntaxe suivante : `{{< get-csv sep="," url="chemin/vers/le/tableau.csv" >}}`. Pour information, le code source du shortcode est accessible [ici](https://gitlab.com/myxonautes/komb/-/blob/master/layouts/shortcodes/get-csv.html)
Le tableau peut être enregistré dans le dossier static/tables comme dans l'exemple donné au-dessus. Si on veut inclure des images dans le tableau, elles doivent être enregistrées dans le dossier static/tables/imgs/ et le nom exact de l'image avec format entré dans une case du tableau csv. Les formats acceptés sont .jpg, .png et .svg.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
